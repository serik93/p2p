$(document).ready(function() {

	if ($('.scroll-pane').length)
		$('.scroll-pane').jScrollPane({autoReinitialise: true});

	if ($('.verify_summ').length)
		$('.verify_summ').text(addSpaces($('.verify_summ').text()));

	$('#totalFee').text(addSpaces($('#totalFee').text()));
	$('#totalSum').text(addSpaces($('#totalSum').text()));

	$('.agree_checkbox').each(function(){
	    $(this).hide().after('<div class="class_checkbox" />');
	});

	$('select').selectmenu()

	$('.class_checkbox').on('click',function(){
	    $(this).toggleClass('checked').prev().prop('checked',$(this).is('.checked'));
	    enableButton();
	});
	$('.checkbox_label').on('click',function(){
	    $('.class_checkbox').toggleClass('checked').prev().prop('checked',$(this).is('.checked'));
	    $('#serviceTermsOk').prop("checked", !$('#serviceTermsOk').prop("checked"));
	    enableButton();

	});


	$('.instruction_item').click(function(){
    	$(this).toggleClass("active");
    	
		$($(this).data('id')).slideToggle("fast");
		hideOthers($(this).data('id'));
		$("html, body").animate({ scrollTop: $('.instructions_block').offset().top }, 800);
	});
	function hideOthers(index){
		$('.instr_content').each(function() {
		    if($(this).attr("id")!=index.substring(1,index.length)){
		    	$(this).hide();
		    }
		});
		$('.instruction_item').each(function() {
			if($(this).data("id")!=index){
		    	$(this).removeClass('active');
		    }
		});
	};

	$('#submitButton').bind('click', false);
	$('#submitButton').css("cursor", "default");
	$('#phoneInput').mask('+7 (999) 999 99 99');

	$('#phoneInput').on('keydown', function(){
		if ($('#phoneInput').val() == '+7 (___) ___ __ __'){
			$(this)[0].setSelectionRange(0, 0);
		}
	});

	$('[data-target-popup]').click(function () {
        $('.mask').show();
        var activePopup = $(this).attr('data-target-popup');
        $(activePopup).show();

    });

    if ($('#sendercard').length)
		$('#sendercard').payment('formatCardNumber');

	if ($('#receivercard').length)
		$('#receivercard').payment('formatCardNumber');
	
	document.querySelector('#receivercard').oninput = function () {
	    formReceiverChecker();
	};
	document.querySelector('#receivercard').onfocus = function () {
	    formReceiverChecker();
	    allow19Digits(true);
	};
	document.querySelector('#receivercard').onkeyup = function () {
		formReceiverChecker();
	};
	document.querySelector('#cardcode').oninput = function () {
	    formSenderChecker();
	};
	document.querySelector('#sendercard').onfocus = function () {
		formSenderChecker();
		allow19Digits(false);
	}
	document.querySelector('#sendercard').oninput = function () {
	    formSenderChecker();
	};
	document.querySelector('.select_list.month').onchange = function () {
	     formSenderChecker();
	};
	document.querySelector('.select_list.year').onchange = function () {
	     formSenderChecker();
	};
	document.querySelector(".select_currency").onchange = function () {
	     formAmountChecker();
	};
	document.querySelector(".select_currency").onfocus = function () {
	     formAmountChecker();
	};
	document.querySelector('#transferAmount').oninput = function () {
		sumSplitter(this);
	    formAmountChecker();
	};
	document.querySelector('#transferAmount').onfocus= function () {
	     formAmountChecker();
	};
	document.querySelector('#transferAmount').keydown= function () {
	     formAmountChecker();
	};

	function sumSplitter(id) {
		var space = id.value.split(" ").join("");

		if (space.length > 0) {
			space = space.split("").reverse().join("");
	        space = split_str(space, 3);
	    }

	    space = space.split("").reverse().join("");
	    id.value = space;
	}
	function addSpaces(sum) {
		var s = sum.split("").reverse();
		var n = [];
		for (var i = 0; i < s.length; i++) {
			if (i > 0 && i % 3 == 0) {
				n.push(' ');
			}	
			n.push(s[i]);
		};
		return n.reverse().join('');
	}
	function getReverse(text){
		return text.split("").reverse().join("")
	}
	function split_str(str, length){
		return str.match(new RegExp('.{1,'+length+'}', 'g')).join(" ");
	}
	var senderComplete = false;
	function formSenderChecker(){
		if (($('#sendercard').val().length == 19) && $('.select_list.month').val().length==2 
			&& $('.select_list.year').val().length==4 && $('#cardcode').val().length==3){
			$('.step.one').css("background", "#e13b00");
			enableInput("#receivercard", true);
			enableInput("#nameReceiver", true);
			enableInput("#lastnameReceiver", true);
			senderComplete = true;
		}
		else {
			$('.step').css("background", "#D9DADA");
			$('.bar').css("background", "#D9DADA");
			enableInput("#receivercard", false);
			//enableInput("#transferAmount", false);
			//enableInput(".select_currency", false);
			$( ".select_currency" ).selectmenu( "disable" );
			senderComplete = false;
			receiverComplete = false;
			amountComplete = false;
		}
	};
	var receiverComplete = false;
	
	function formReceiverChecker(){
		if ($('#receivercard').val().length == 20 || $('#receivercard').val().length == 23) {
			$('.step.two').css("background", "#e13b00");
			$('.bar.one').css("background", "#e13b00");
			enableInput("#transferAmount", true);
			//enableInput(".select_currency", true);
			$( ".select_currency" ).selectmenu( "enable" );
			receiverComplete = true;
		}
		else {
			$('.step.two').css("background", "#D9DADA");
			$('.bar.one').css("background", "#D9DADA");
			$('.step.three').css("background", "#D9DADA");
			$('.bar.two').css("background", "#D9DADA");
			enableInput("#transferAmount", false);
			//enableInput(".select_currency", false);
			$( ".select_currency" ).selectmenu( "disable" );
			receiverComplete = false;
			amountComplete = false;
		}
	};
	function enableInput(id, bool){
		if(bool==true){
			$(id).prop('disabled', false);
		} else {
			$(id).prop('disabled', true);
		}
	};
	var amountComplete = false;
	function formAmountChecker() {
		if($('#transferAmount').val().length!=0){
			$('.step.three').css("background", "#e13b00");
			$('.bar.two').css("background", "#e13b00");
			amountComplete = true;
		}
		else {
			$('.step.three').css("background", "#D9DADA");
			$('.bar.two').css("background", "#D9DADA");
			amountComplete = false;
		}
	};
	function enableButton(){
		if($(".class_checkbox").is(".checked")==true) {
				$('#submitButton').unbind('click', false);
				$('#submitButton').css("cursor", "pointer");
		} else {
			$('#submitButton').bind('click', false);
			$('#submitButton').css("cursor", "default");
		}
		
	};

	var regExp = /[0-9]/;
	$('#receivercard').on('keydown keyup', onlyNums);
	$('#transferAmount').on('keydown keyup', onlyNums);
	$('#cardcode').on('keydown keyup', onlyNums);
	$('#codeLookUp').on('keydown keyup', onlyNums);
	$('.nameReceiver').on('keypress', onlyLetters);
	function onlyNums(e) {
		var value = String.fromCharCode(e.which) || e.key;
		//console.log(e);
		// Only numbers, dots and commas
		if (!regExp.test(value)
		  && e.which != 8   // backspace
		  && e.which != 46  // delete
		  && (e.which < 37  // arrow keys
		    || e.which > 40)
		  && (e.which <96
		  	|| e.which > 105)) {
		      e.preventDefault();
		      return false;
		}
	}
	function onlyLetters(event){

	    var englishAlphabetAndWhiteSpace = /[A-Za-z]/g;

	    var key = String.fromCharCode(event.which);

	    if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetAndWhiteSpace.test(key)) {
	        return true;
	    }

	    return false;
	}

	function allow19Digits(bool){
		if (bool)
			$.payment.cards.unshift(
				{
					type: 'mastercard',
					// Array of prefixes used to identify the card type.
					patterns: [
				    	22, 23, 24, 25, 26, 27, 51, 52, 53, 54, 55, 41, 42, 43, 44, 45
				  	],

					length: [19],

					cvcLength: [3],
					// Boolean indicating whether a valid card number should satisfy the Luhn check.
					luhn: true,
					// Regex used to format the card number. Each match is joined with a space.
					format: /(\d{1,4})/g
				}
			)
		else
			$.payment.cards.unshift({

				type: 'mastercard',
				// Array of prefixes used to identify the card type.
				patterns: [
			    	51, 52, 53, 54, 55,
			    	22, 23, 24, 25, 26, 27, 41, 42, 43, 44, 45
			  	],

				length: [16],

				cvcLength: [3],
				// Boolean indicating whether a valid card number should satisfy the Luhn check.
				luhn: true,
				// Regex used to format the card number. Each match is joined with a space.
				format: /(\d{1,4})/g

			})
	}

});

function closePopup(context){
	$(context).closest('.popup').hide();
	$('.mask').hide();
}
